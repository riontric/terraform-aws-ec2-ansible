Test Task DevOps
-------------
- export AWS credentials \ region: <br>
```$ export AWS_ACCESS_KEY_ID="anaccesskey"``` <br>
```$ export AWS_SECRET_ACCESS_KEY="asecretkey"``` <br>
```$ export AWS_DEFAULT_REGION="region"``` <br>
- terraform create ec2 instance with security group and key pair: <br>
Use this commands in terraform/ folder <br>
```$ terraform init``` <br>
```$ terraform apply``` <br>
- ansible connect to remote hosts: <br>
Use this commands in ansible/ folder <br>
```$ ansible-inventory -i aws_ec2.yml --list``` <br>
```$ ansible aws_ec2 -i aws_ec2.yml -m ping --user=ubuntu``` <br>
```$ ansible-playbook -i aws_ec2.yml playbook-default.yml``` <br>
```$ ansible-playbook -i aws_ec2.yml playbool-nginx.yml``` <br>
- ec2 without desktop, autorun nginx in docker was successful: <br>
Look this using ```public_ip_address_ec2``` with port ```8080```.
provider "aws" {} #use ../credentials/aws_creds in ur environment

resource "aws_instance" "ec2-instance" {
  count                  = 2
  ami                    = "ami-0015a39e4b7c0966f" #Linux Ubuntu 20.04 AMI
  instance_type          = "t3.micro"
  key_name               = "authorized_keys"
  vpc_security_group_ids = [aws_security_group.allow_tls.id]
  user_data              = <<EOF
#!/bin/bash
apt update && apt upgrade
myip=`curl http://169.254.169.254/latest/meta-data/public-ipv4`
mkdir -p /home/ubuntu/webproject/www
mkdir -p /home/ubuntu/webproject/nginx_logs
echo "<h2>Hello from NGINX in Docker!</h2><br>Public IP EC2: $myip" > /home/ubuntu/webproject/www/index.html
EOF
}

resource "aws_key_pair" "authorized_keys" {
  key_name   = "authorized_keys"
  public_key = file("../ansible/ssh/authorized_keys.pem")
}

resource "aws_security_group" "allow_tls" {
  name = "Ununtu Security Group"

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port        = 5901
    to_port          = 5901
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
